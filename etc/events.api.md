## API Report File for "@pyramine/events"

> Do not edit this file. It is a report generated by [API Extractor](https://api-extractor.com/).

```ts

// @public (undocumented)
export type EventArgs<EventMapType extends EventMap, EventName extends keyof EventMapType> = EventMapType[EventName];

// @public (undocumented)
export class EventHub<EventMapType extends EventMap> {
    // (undocumented)
    emit<EventName extends keyof EventMapType>(event: EventName, ...args: EventArgs<EventMapType, EventName>): void;
    // (undocumented)
    off<EventName extends keyof EventMapType>(event: EventName, callback: EventListener_2<EventMapType, EventName>): void;
    // (undocumented)
    on<EventName extends keyof EventMapType>(event: EventName, callback: EventListener_2<EventMapType, EventName>): void;
    // (undocumented)
    once<EventName extends keyof EventMapType>(event: EventName, callback: EventListener_2<EventMapType, EventName>): void;
}

// @public (undocumented)
type EventListener_2<EventMapType extends EventMap, EventName extends keyof EventMapType> = (...args: EventArgs<EventMapType, EventName>) => void;
export { EventListener_2 as EventListener }

// @public (undocumented)
export interface EventMap {
    // (undocumented)
    [Key: string]: unknown[];
}

// (No @packageDocumentation comment for this package)

```
