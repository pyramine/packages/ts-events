/**
 * @public
 */
export interface EventMap {
    [Key: string]: unknown[];
}

/**
 * @public
 */
export type EventListener<
    EventMapType extends EventMap,
    EventName extends keyof EventMapType,
> = (...args: EventArgs<EventMapType, EventName>) => void;

/**
 * @public
 */
export type EventArgs<
    EventMapType extends EventMap,
    EventName extends keyof EventMapType,
> = EventMapType[EventName];

/**
 * @public
 */
export class EventHub<EventMapType extends EventMap> {
    private _listeners: {
        [EventName in keyof EventMapType]?: Set<
            EventListener<EventMapType, EventName>
        >;
    } = {};

    private getListeners<EventName extends keyof EventMapType>(
        event: EventName,
    ): Set<EventListener<EventMapType, EventName>> {
        // we can safely use || here, as we only have "Set" objects here or undefined.
        // we do not have other falsy values here, which would trigger the fallback.
        return this._listeners[event] || (this._listeners[event] = new Set());
    }

    public on<EventName extends keyof EventMapType>(
        event: EventName,
        callback: EventListener<EventMapType, EventName>,
    ): void {
        this.getListeners(event).add(callback);
    }

    public once<EventName extends keyof EventMapType>(
        event: EventName,
        callback: EventListener<EventMapType, EventName>,
    ): void {
        const onceCallback = ((...args: EventMapType[EventName]) => {
            this.off(event, onceCallback);
            callback.call(null, ...args);
        }) as EventListener<EventMapType, EventName>;

        this.on(event, onceCallback);
    }

    public off<EventName extends keyof EventMapType>(
        event: EventName,
        callback: EventListener<EventMapType, EventName>,
    ): void {
        this.getListeners(event).delete(callback);

        // remove Set for event to prevent memory leaks
        if (this.getListeners(event).size <= 0) {
            delete this._listeners[event];
        }
    }

    public emit<EventName extends keyof EventMapType>(
        event: EventName,
        ...args: EventArgs<EventMapType, EventName>
    ): void {
        for (const eventListener of this.getListeners(event)) {
            eventListener.call(null, ...args);
        }
    }
}
