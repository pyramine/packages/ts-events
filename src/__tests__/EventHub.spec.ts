import { EventHub, EventMap } from '../EventHub';

interface TestEventMap extends EventMap {
    one: [event: string];
    two: [event: number];
}

it('should listen for events', () => {
    const eventHub = new EventHub<TestEventMap>();

    const eventCallback: (event: string) => void = jest.fn();
    eventHub.on('one', eventCallback);
    expect(eventCallback).not.toHaveBeenCalled();

    eventHub.emit('one', 'myValue');
    expect(eventCallback).toHaveBeenCalledWith('myValue');
});

it('should listen for events multiple times', () => {
    const eventHub = new EventHub<TestEventMap>();

    const eventCallback: (event: string) => void = jest.fn();
    eventHub.on('one', eventCallback);
    expect(eventCallback).not.toHaveBeenCalled();

    eventHub.emit('one', 'myValue');
    expect(eventCallback).toHaveBeenCalledTimes(1);
    expect(eventCallback).toHaveBeenCalledWith('myValue');

    eventHub.emit('one', 'myValue2');
    expect(eventCallback).toHaveBeenCalledTimes(2);
    expect(eventCallback).toHaveBeenCalledWith('myValue2');
});

it('should emit events to multiple listeners', () => {
    const eventHub = new EventHub<TestEventMap>();

    const eventCallback1: (event: string) => void = jest.fn();
    const eventCallback2: (event: string) => void = jest.fn();
    eventHub.on('one', eventCallback1);
    eventHub.on('one', eventCallback2);

    expect(eventCallback1).not.toHaveBeenCalled();
    expect(eventCallback2).not.toHaveBeenCalled();

    eventHub.emit('one', 'myValue');

    expect(eventCallback1).toHaveBeenCalledWith('myValue');
    expect(eventCallback2).toHaveBeenCalledWith('myValue');
});

it('should stop listening for events', () => {
    const eventHub = new EventHub<TestEventMap>();

    const eventCallback: (event: string) => void = jest.fn();
    eventHub.on('one', eventCallback);
    expect(eventCallback).not.toHaveBeenCalled();

    eventHub.emit('one', 'myValue');
    expect(eventCallback).toHaveBeenCalledTimes(1);
    expect(eventCallback).toHaveBeenCalledWith('myValue');

    eventHub.off('one', eventCallback);
    eventHub.emit('one', 'myValue1');
    expect(eventCallback).toHaveBeenCalledTimes(1);
    expect(eventCallback).not.toHaveBeenCalledWith('myValue1');
});

it('should only listen for events once', () => {
    const eventHub = new EventHub<TestEventMap>();

    const eventCallback: (event: string) => void = jest.fn();
    eventHub.once('one', eventCallback);
    expect(eventCallback).not.toHaveBeenCalled();

    eventHub.emit('one', 'myValue');
    expect(eventCallback).toHaveBeenCalledTimes(1);
    expect(eventCallback).toHaveBeenCalledWith('myValue');

    eventHub.emit('one', 'myValue1');
    expect(eventCallback).toHaveBeenCalledTimes(1);
    expect(eventCallback).not.toHaveBeenCalledWith('myValue1');
});

it('should remove empty sets from memory to prevent memory leaks', () => {
    const eventHub = new EventHub<TestEventMap>();
    expect(eventHub).toMatchSnapshot();

    const eventCallback: (event: string) => void = jest.fn();
    eventHub.on('one', eventCallback);
    expect(eventHub).toMatchSnapshot();

    eventHub.off('one', eventCallback);
    expect(eventHub).toMatchSnapshot();
});
