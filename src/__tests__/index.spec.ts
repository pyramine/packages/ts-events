import * as index from '../index';

it('should provide consistent api', () => {
    expect(index).toMatchInlineSnapshot(`
        {
          "EventHub": [Function],
        }
    `);
});
